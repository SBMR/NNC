# Neural Network Causality Test

## Overview

In this project, we will see how to expand Granger's Causality detection through the use of Artificial Neural Network. 

## Getting Started

These instructions will show you how to use/customize this library.

### Prerequisites

What things you need to install the software and how to install them

```
TensorFlow Python API r1.8
Scipy (0.18 or later)
Numpy (1.11 or later)
statsmodels (0.6 or later)
sklearn (0.17 or later)
```


## Running the tests

You will find an [Ipython Notebook](https://gitlab.com/SBINX/NNC/blob/master/example_use.ipynb) that will explain how to use this library. 
It will:
* Compare the results of NNC against Granger causality.
* Use NNC library with a customized Neural Network model. 

## Authors

* **Seddik BELKOURA** 


## Future Work

This work is still in progress. The result shown are very encouraging. The author
is currently working on:
* a multi-dimensional version 
* An efficient early stop trigger when causality is obvious without introducing too much delay otherwise (no causality).
* Optimization of the code (e.g. ingestion of data)

Note that you can find in the folder previous_version the original code which indeed included an early stop - yet non-optimised. 


## License

Please cite this work: 

S. Belkoura, M, Zanin, A. LaTorre. "NNC: a Neural Network Causality library". Unpublished.
