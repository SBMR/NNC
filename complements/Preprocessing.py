
from statsmodels.tsa.tsatools import lagmat2ds
import numpy as np
from sklearn import preprocessing
from sklearn.utils import shuffle




class information():

    def __init__(self, X, Y, maxlag = 1):

        self.X = X
        self.Y = Y
        self.maxlag = maxlag

    def shifted_matrix(self):

        fullTS = np.hstack((self.Y,self.X))
        self.data = lagmat2ds(fullTS, self.maxlag, maxlagex=self.maxlag, trim='both', dropex=1)

    def pre_process(self, train_size=0.8):
        """
        Note: input AND output are normalized the same way, being contiuous TS.
        """

        enough_test_data = False
        while not enough_test_data:
            mask = np.random.rand(self.data.shape[0]) < train_size
            enough_test_data = np.sum(mask) <= (train_size + 0.05) * self.data.shape[0]

        Trn = 2*preprocessing.MinMaxScaler().fit_transform(self.data[mask,:])-1
        Tst = 2 * preprocessing.MinMaxScaler().fit_transform(self.data[~mask,:]) - 1
        #Trn = preprocessing.StandardScaler().fit(self.data[mask,:]).transform(self.data[mask,:])
        #Tst = preprocessing.StandardScaler().fit(self.data[mask, :]).transform(self.data[mask, :])


        self.TrnInp = Trn[:,1:]
        self.TstInp = Tst[:,1:]
        self.TrnOut = Trn[:,0].reshape(-1,1)
        self.TstOut = Tst[:,0].reshape(-1,1)

    def permute(self, lag):

        datalake = [data_ingest(self.TrnInp,self.TrnOut,self.TstInp, self.TstOut)]
        for l in range(lag):
            copy = self.TrnInp.copy()
            copy[:,-l-1] = shuffle(copy[:,-l-1],random_state=l)
            datalake.append( data_ingest(copy,self.TrnOut,self.TstInp, self.TstOut) )

        return datalake

    def permute_boost(self, all=False):

        copy = self.TrnInp.copy()
        if all:
            copy[:,-self.maxlag:] = shuffle(copy[:,-self.maxlag:],random_state=1)

        return data_ingest(copy,self.TrnOut,self.TstInp, self.TstOut)

class data_ingest():

    def __init__(self, TrnInp, TrnOut, TstInp, TstOut):
        self.index_in_epoch = 0
        self.epochs_completed = 0
        self.TrnInp = TrnInp
        self.TrnOut = TrnOut
        self.TstInp = TstInp
        self.TstOut = TstOut

    def next_batch(self, batch_size):
        """
        Return an array of batch_size random data points from our training and testing sets

        """
        start = self.index_in_epoch
        self.index_in_epoch += batch_size

        if self.index_in_epoch > self.TrnInp.shape[0]:
            self.epochs_completed += 1
            perm = np.arange(self.TrnInp.shape[0])
            np.random.shuffle(perm)
            self.TrnInp = self.TrnInp[list(perm),:]
            self.TrnOut = self.TrnOut[perm,:]
            start = 0
            self.index_in_epoch = batch_size
            assert batch_size <= self.TrnInp.shape[0]
        end = self.index_in_epoch

        return self.TrnInp[start:end, :], self.TrnOut[start:end, :]

