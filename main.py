


import scipy.stats as stats
import numpy as np
import tensorflow as tf
from time import gmtime, strftime, time
from complements.Preprocessing import information





def test(X, Y, maxlag=1, train_size=0.8, nb_neurons=10, learn_rate=1e-3,
         max_epoch=10000, alpha=0.01,
         verbose=False, custom=False, save=False, **kwargs):

    tf.reset_default_graph()

    if custom:
        model = kwargs['model']
    else:
        from complements.basic_model import model

    assert X.shape == Y.shape
    assert X.shape[1] == 1
    assert Y.shape[1] == 1

    start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    duration = time()


    data = information(X, Y, maxlag)
    data.shifted_matrix()
    data.pre_process(train_size=train_size)
    datalake = data.permute(maxlag)

    batch_size = max(1, int(np.floor((1 - train_size) * data.TrnInp.shape[0] / 10)))

    p_values_mean = np.ones((1, maxlag))


    main_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate, save=save)
    control_models = [model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate,
                            index=i + 1, reuse=True, save=save) for i in range(maxlag)]


    main_model.initialize_model()
    main_model.train(datalake[0], max_epoch, batch_size, path='./checkpoints/main_model')
    baseline_SE = main_model.restore(data.data)


    for i, control_model in enumerate(control_models):
        control_model.initialize_model()
        control_model.train(datalake[i + 1], max_epoch, batch_size, path='./checkpoints/control_model_' + str(i+1))
        control_SE = control_model.restore(data.data, path='./checkpoints/control_model_' + str(i+1))

        p_values_mean[0, i] = stats.ttest_ind(baseline_SE,control_SE,equal_var=False).pvalue[0]


    p_value = np.min(p_values_mean, axis=1)[0]
    offset = np.nan
    if p_value < alpha:
        offset = maxlag - np.argmin(p_values_mean)

    end_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    duration = time() - duration

    tf.reset_default_graph()

    if verbose:
        print("Neural Network Causality")
        print("p-value =", p_value)
        print("offset =", offset)
        print("Computation lasted from ", start_time, ' to ', end_time)

    return {"pvalue":p_value, "offset":offset, "duration":duration,
            "epochs_completed":[dl.epochs_completed for dl in datalake],
            "main_model":main_model, "control_models":control_models}


def test_boost(X, Y, maxlag=1, train_size=0.8, nb_neurons=10, learn_rate=1e-3,
                    max_epoch = 10000, alpha = 0.01, verbose=False,
               custom=False, save=False, **kwargs):

        tf.reset_default_graph()

        if custom:
            model = kwargs['model']
        else:
            from complements.basic_model import model


        assert X.shape == Y.shape
        assert X.shape[1] == 1
        assert Y.shape[1] == 1



        start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        duration = time()


        data = information(X, Y, maxlag)
        data.shifted_matrix()
        data.pre_process(train_size = train_size)
        data_not_permuted = data.permute_boost(all=False)
        data_permuted = data.permute_boost(all=True)

        batch_size = max(1, int(np.floor((1-train_size)*data.TrnInp.shape[0] / 10)))


        main_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate, save=save)
        control_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate,
                                index=1, reuse=True, save=save)



        main_model.initialize_model()
        main_model.train(data_not_permuted, max_epoch, batch_size, path='./checkpoints/main_model')
        baseline_SE = main_model.restore(data.data)


        control_model.initialize_model()
        control_model.train(data_permuted, max_epoch, batch_size, path='./checkpoints/boosted_control_model')
        control_SE = main_model.restore(data.data)

        p_value = stats.ttest_ind(baseline_SE,control_SE,equal_var=False).pvalue[0]


        end_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        duration = time() - duration

        tf.reset_default_graph()

        if verbose:
            print("Neural Network Causality")
            print("p-value =", p_value)
            print("Computation lasted from ", start_time,' to ',end_time)

        return {"pvalue":p_value, "duration":duration,
                "epochs_completed":[data_permuted.epochs_completed,data_not_permuted.epochs_completed],
                "main_model":main_model,"control_model": control_model}
