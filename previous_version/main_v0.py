


import scipy.stats as stats
import numpy as np
import tensorflow as tf
from time import gmtime, strftime, time
from complements.Preprocessing import information





def test(X, Y, maxlag=1, train_size=0.8, nb_neurons=10, learn_rate=1e-3, init_epoch=2000,
         increment=1000, max_epoch=10000, alpha=0.01, false_stop_threshold=0.9,
         verbose=False, custom=False, **kwargs):
    tf.reset_default_graph()

    if custom:
        model = kwargs['model']
    else:
        from complements.basic_model import model

    assert X.shape == Y.shape
    assert X.shape[1] == 1
    assert Y.shape[1] == 1

    start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    duration = time()
    epochs_completed = 0

    data = information(X, Y, maxlag)
    data.shifted_matrix()
    data.pre_process(train_size=train_size)
    batch_size = max(1, int(np.floor((1 - train_size) * data.TrnInp.shape[0] / 10)))
    p_values = np.ones((1, maxlag))

    main_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate)
    control_models = [model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate,
                            index=i + 1, reuse=True) for i in range(maxlag)]

    datalake = data.permute(maxlag)

    main_model.initialize_model()
    main_model.train(datalake[0], init_epoch, batch_size)

    for i, control_model in enumerate(control_models):
        control_model.initialize_model()
        control_model.train(datalake[i + 1], init_epoch, batch_size)
        error = np.hstack((main_model.prediction, control_model.prediction))
        t = (np.mean(error[:, 0]) - np.mean(error[:, 1])) / np.sqrt(
            (np.std(error[:, 0]) ** 2 + np.std(error[:, 1]) ** 2) / error.shape[0])
        p = stats.t.cdf(x=t, df=2 * (error.shape[0] - 1))
        p_values[0, i] = p

    epochs_completed += init_epoch
    ratio = np.mean(main_model.error_track[-batch_size:]) / np.mean(main_model.error_track[:batch_size])

    control_ratios = np.zeros((len(control_models), 1))

    while ratio < 1:

        if ((np.sum(p_values < alpha) == 1) * control_ratios[np.argmin(p_values), 0]):
            break

        main_model.train(datalake[0], increment, batch_size)

        for i, control_model in enumerate(control_models):
            control_model.train(datalake[i + 1], increment, batch_size)
            error = np.hstack((main_model.prediction, control_model.prediction))
            t = (np.mean(error[:, 0]) - np.mean(error[:, 1])) / np.sqrt(
                (np.std(error[:, 0]) ** 2 + np.std(error[:, 1]) ** 2) / error.shape[0])
            p = stats.t.cdf(x=t, df=2 * (error.shape[0] - 1))
            p_values[0, i] = p
            control_ratios[i, 0] = np.mean(control_model.error_track[-batch_size:]) / np.mean(
                control_model.error_track[-increment - batch_size:-increment]) > false_stop_threshold

        epochs_completed += increment

        ratio = np.mean(main_model.error_track[-batch_size:]) / np.mean(
            main_model.error_track[-increment - batch_size:-increment])

        if epochs_completed > max_epoch:
            if verbose:
                print('Maximum epochs allowed reached without convergence')
            break

    p_value = np.min(p_values, axis=1)[0]
    offset = np.nan
    if p_value < alpha:
        offset = maxlag - np.argmin(p_values)

    end_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
    duration = time() - duration

    tf.reset_default_graph()

    if verbose:
        print("Neural Network Causality")
        print("p-value =", p_value)
        print("offset =", offset)
        print("Computation lasted from ", start_time, ' to ', end_time)

    return p_value, offset, duration, epochs_completed, data, main_model, control_models


def test_boost(X, Y, maxlag=1, train_size=0.8, nb_neurons=10, learn_rate=1e-3, false_stop_threshold = 0.9,
                    init_epoch = 2000, increment = 1000, max_epoch = 10000, alpha = 0.01, verbose=False,
               custom=False, **kwargs):

        tf.reset_default_graph()

        if custom:
            model = kwargs['model']
        else:
            from complements.basic_model import model


        assert X.shape == Y.shape
        assert X.shape[1] == 1
        assert Y.shape[1] == 1



        start_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        duration = time()
        epochs_completed = 0

        data = information(X, Y, maxlag)
        data.shifted_matrix()
        data.pre_process(train_size = train_size)
        batch_size = max(1, int(np.floor((1-train_size)*data.TrnInp.shape[0] / 10)))
        p_values = np.ones((1,maxlag))

        main_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate)
        control_model = model(data.TrnInp, nb_neurons=nb_neurons, learn_rate=learn_rate,
                                index=1, reuse=True)

        data_not_permuted = data.permute_boost(all=False)
        data_permuted = data.permute_boost(all=True)

        main_model.initialize_model()
        main_model.train(data_not_permuted, init_epoch, batch_size)

        control_model.initialize_model()
        control_model.train(data_permuted, init_epoch, batch_size)
        error = np.hstack((main_model.prediction, control_model.prediction))
        t = (np.mean(error[:, 0]) - np.mean(error[:, 1])) / np.sqrt(
                (np.std(error[:, 0]) ** 2 + np.std(error[:, 1]) ** 2) / error.shape[0])
        p = stats.t.cdf(x=t, df=2 * (error.shape[0] - 1))
        p_value = p
        #p_value = scipy.stats.ttest_1samp(error[:,0]-error[:,1],0).pvalue

        epochs_completed += init_epoch
        ratio = np.mean(main_model.error_track[-batch_size:]) / np.mean(main_model.error_track[-increment:-increment+batch_size])
        control_ratio = np.mean(control_model.error_track[-batch_size:]) / np.mean(
            control_model.error_track[:batch_size])


        while ratio < 1:

            if (p_value < alpha) & (control_ratio > false_stop_threshold):
                break

            main_model.train(data_not_permuted, increment, batch_size)

            control_model.train(data_permuted, increment, batch_size)
            error = np.hstack((main_model.prediction, control_model.prediction))
            t = (np.mean(error[:, 0]) - np.mean(error[:, 1])) / np.sqrt(
                    (np.std(error[:, 0]) ** 2 + np.std(error[:, 1]) ** 2) / error.shape[0])
            p = stats.t.cdf(x=t, df=2 * (error.shape[0] - 1))
            p_value = p
            #p_value = scipy.stats.ttest_1samp(error[:, 0] - error[:, 1], 0).pvalue

            epochs_completed += increment
            control_ratio = np.mean(control_model.error_track[-batch_size:]) / np.mean(
                control_model.error_track[-increment-batch_size:-increment])
            ratio = np.mean(main_model.error_track[-batch_size:]) / np.mean(main_model.error_track[-increment-batch_size:-increment])

            if epochs_completed > max_epoch:
                if verbose:
                    print('Maximum epochs allowed reached without convergence')
                break


        end_time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        duration = time() - duration

        tf.reset_default_graph()

        if verbose:
            print("Neural Network Causality")
            print("p-value =", p_value)
            print("Computation lasted from ", start_time,' to ',end_time)

        return p_value, duration, start_time, end_time, epochs_completed ,data, main_model, control_model
